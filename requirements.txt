aiohttp==3.9.1
aiortc==1.5.0
opencv-python==4.10.0.82
numpy==1.21.5
av==10.0.0
Jinja2==3.0.3
