# ENTREGA CONVOCATORIA JUNIO
###### Nombre: Lucía Izquierdo Riolobos
###### Correo de la universidad: l.izquierdo.2019@alumnos.urjc.es
###### Enlace al vídeo de demostración del proyecto: https://youtu.be/n6NloxMUXzA

## PARTE BÁSICA
Este programa implementa un sistema de transmisión de video en tiempo real utilizando WebRTC, la parte básica funcina todo correctamente, aunque es importante utilizar Chrome como navegador ya que en Firefox no se ven los videos.
* Primero, se inicia el sistema de señalización en el puerto designado para señalización y se configuran los cuatro transmisores de video que transmiten videos de planetas.
* Durante la inicialización, los transmisores envían mensajes de registro al sistema de señalización, proporcionando su información.
* Se configura el servidor frontal en el puerto HTTP y se abre el navegador con la URL proporcionada.
* Cuando un usuario hace clic en el botón "start" para reproducir cualquiera de los cuatro videos, el archivo HTML envía una solicitud de oferta (offer) al servidor frontal, que la retransmite al sistema de señalización. El sistema de señalización pasa la oferta al transmisor correspondiente, que responde con una respuesta (answer). Esta respuesta se envía de vuelta al servidor frontal y luego al navegador, estableciendo una conexión WebRTC que permite la reproducción del video en el navegador. Estos archivos sdp se guardan en la carpeta sdp_files. 
* * * 
Es importante recalcar que aunque el formato de mensajes históricos es el correcto, hay más tipos también.

## PARTE ADICIONAL
* Información adicional para los ficheros: Cuando los streamers se registren, enviarán al servidor de señalización información adicional. Esta información incluye un título y un resumen en texto del fichero. Esta información se almacena en el directorio del servidor de señalización, que se la pasara al servidor frontal cuando éste le pida un listado, para poder mostrársela a los navegadores.



